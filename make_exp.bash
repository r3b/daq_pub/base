#!/bin/bash

if [ ! "$1" ]
then
	echo "Example: $0 202402_s118" 1>&2
	exit 1
fi
exp_name=$1
mkdir -p $exp_name
cd $exp_name
[ -d env ] || git clone https://git.gsi.de/r3b/daq_pub/env.git
[ -d scripts ] || git clone https://git.gsi.de/r3b/daq_pub/scripts.git
[ -d trloii ] || git clone https://git.chalmers.se/expsubphys/trloii.git
[ -d drasi ] || git clone https://git.chalmers.se/expsubphys/drasi.git
[ -d nurdlib ] || git clone https://git.chalmers.se/expsubphys/nurdlib.git
echo
echo "Done! Directory = $(pwd)"
echo
echo "Next you prepare mybranch.conf and run"
echo "scripts/daq_create_links.bash and scripts/mybranch_match.bash!"
